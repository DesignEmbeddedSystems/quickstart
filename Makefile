

# Target specific macros
TARGET = helloworld
TARGET_SOURCES := helloworld.c
TOPPERS_OSEK_OIL_SOURCE := ./helloworld.oil

FIRMWARE:=/cygdrive/c/nexttool/nxtFirmware/lms_arm_nbcnxc_132_20130303_2051_nxt_enhanced_firmware.rfwx

# extra include paths  (for .h header files)
USER_INC_PATH= .

#TIPS:
# * make -n <target>  :   instead of running commands it prints them instead$
#                         -> no commands are run at all!!  -> dry run$
#  eg.
#    make clean ;  make -n > make-n.txt
#     => study make-n.txt in editor to see what commands are executed
#
# * when building also for each c/c++ file a .d dependency file is build.
#   eg. for rover.c a dependency file  build/rover.d is made which contains
#       all include files rover.c depends on!
#
# * make -d  : gives debug output -> shows exactly the decisions make makes!$
#

###############################################

ROOT := /cygdrive/c/cygwin/nxtOSEK
GNUARM_ROOT = /cygdrive/c/cygwin/GNUARM
NEXTTOOL_ROOT = /cygdrive/c/cygwin/nexttool
#note: sg.exe must be manually installed at $ROOT/toppers_osek/sg/sg.exe
#      you can download it from http://lejos-osek.sourceforge.net/download.htm


# build mode specifies what kind of executable you want to build:
# either :
#   * ROM_ONLY   : loading using biosflash appflash
#   * RAM_ONLY   : loading using ramboot
#   * RXE_ONLY   : loading using rxeflash
# note: if BUILD_MODE undefined than all three are build.
BUILD_MODE = RXE_ONLY

###############################################

ECROBOT_DEF = LEJOS_OSEK INTEGER_CODE 

O_PATH ?= build


# make help the default target by making it the first target in the file
.PHONY: help
help:
	@echo "makefile with following targets:"
	@echo "  clean                      - deletes all build files"
	@echo "  all                        - builds program into a .rxe file"
	@echo "  "
	@echo "  mostlyclean                - deletes all build files except the from oil file generated source files"
	@echo "  generate_source_from_oil   - generate source from the oil file"
	@echo "  delete_source_from_oil     - deletes all from the oil file generated source files"
	@echo "  "
	@echo "NeXTTool commands:  "
	@echo "  nxt_upload                 - upload the build .rxe file to the nxt"
	@echo "  nxt_delete                 - delete the build .rxe file from the nxt"
	@echo "  nxt_clear                  - delete all programs on nxt"
	@echo "  nxt_list                   - list all programs on nxt"
	@echo "  nxt_freemem                - query free memory on nxt (flash storage)"
	@echo "  nxt_battery                - get battery level status"
	@echo "  nxt_firmware               - upload firmware"
	@echo "  nxt_version                - version of firmware on nxt "
	@echo "  "
	@echo "TIP: run make with option -n and then instead of executing the commands"
	@echo "     it will print the commands. (doing a dry run to show the commands)"
	@echo "     eg."
	@echo "         make -n nxt_version"



# add an extra dependency for this all target.
# note: all target is defined multiple times, but that is not a problem as long
#       no more than one of them as commands!
.PHONY: all
all:  check_for_sg

.PHONY: generate_source_from_oil
generate_source_from_oil: $(TOPPERS_CFG_SOURCE) $(TOPPERS_CFG_HEADER) implementation.oil

.PHONY: delete_source_from_oil
delete_source_from_oil:
	@echo "Removing kernel config files"
	@rm -f $(TOPPERS_CFG_SOURCE)
	@rm -f $(TOPPERS_CFG_HEADER)
	@rm -f $(COM_CFG_SOURCE)
	@rm -f $(COM_CFG_HEADER)
	@rm -f implementation.oil
	@rm -f *.dat
	@rm -f tmpfile1

.PHONY: mostlyclean
mostlyclean:
	@echo "Removing objects"
	@rm -rf $(O_PATH)
	@echo "Removing targets"
	@rm -f $(ALL_TARGETS)
	@echo "Removing map files"
	@rm -f *.map
	@echo "Removing upload scripts"
	@rm -f *.sh

.PHONY: upload
oldupload: all
	./rxeflash.sh


nxt_upload: check_for_rxe
	@echo "Executing NeXTTool to upload $(RXEBIN_TARGET)..."
	@$(LAUNCHER) $(NEXTTOOL_ROOT)/$(NEXTTOOL) /COM=usb -download=$(RXEBIN_TARGET)
	@$(LAUNCHER) $(NEXTTOOL_ROOT)/$(NEXTTOOL) /COM=usb -listfiles=$(RXEBIN_TARGET)
	@echo "NeXTTool is terminated."

nxt_delete:
	@echo "Executing NeXTTool to delete $(RXEBIN_TARGET)..."
	@$(LAUNCHER) $(NEXTTOOL_ROOT)/$(NEXTTOOL) /COM=usb -delete=$(RXEBIN_TARGET)


nxt_firmware: check_for_firmware
	@echo "Executing NeXTTool to upload $(FIRMWARE)."
	@$(LAUNCHER) $(NEXTTOOL_ROOT)/$(NEXTTOOL) /COM=usb -firmware=$(FIRMWARE)

.PHONY: check_for_firmware
check_for_firmware:
	test -f $(FIRMWARE) || { echo "Error: missing $(FIRMWARE) file." && echo "Configure the FIRMWARE variable in this Makefile correctly" && echo "You can download new firmware from: http://bricxcc.sourceforge.net/test_releases/." && echo "More info you find at: http://bricxcc.sourceforge.net/firmware.html" && exit 1; }

.PHONY: check_for_rxe
check_for_rxe:
	test -f $(RXEBIN_TARGET) || { echo "Error: missing $(RXEBIN_TARGET) file." && echo "First build $(RXEBIN_TARGET) file using 'make all'!" && exit 1; }


nxt_freemem:
	@$(LAUNCHER) $(NEXTTOOL_ROOT)/$(NEXTTOOL) /COM=usb -freemem

nxt_clear:
	@$(LAUNCHER) $(NEXTTOOL_ROOT)/$(NEXTTOOL) /COM=usb -clear

nxt_list:
	@$(LAUNCHER) $(NEXTTOOL_ROOT)/$(NEXTTOOL) /COM=usb -listfiles

nxt_version:
	@$(LAUNCHER) $(NEXTTOOL_ROOT)/$(NEXTTOOL) /COM=usb -versions


nxt_battery:
	@$(LAUNCHER) $(NEXTTOOL_ROOT)/$(NEXTTOOL) /COM=usb -battery


.PHONY: info
info:
	@echo ""
	@echo "input/output files"
	@echo "------------------"
	@echo ""
	@echo "Building: $(ALL_TARGETS)"
	@echo ""
	@echo "C sources: $(C_SOURCES)"
	@echo ""
	@echo "C objects to build: $(C_OBJECTS)"
	@echo ""
	@echo "C++ sources: $(CC_SOURCES)"
	@echo ""
	@echo "C++ objects to build: $(CC_OBJECTS)"
	@echo ""
	@echo "Assembler sources: $(S_SOURCES) to $(S_OBJECTS)"
	@echo ""
	@echo "LD source: $(LDSCRIPT_SOURCE)"
	@echo ""
	@echo ""
	@echo ""
	@echo "gcc/g++ building settings"
	@echo "-------------------------"
	@echo ""
	@echo "all include paths: gcc/g++ default include paths + INC_PATH + . + TOPPERS_INC_PATH"
	@echo ""
	@echo "INC_PATH: $(INC_PATH)"
	@echo ""
	@echo "TOPPERS_INC_PATH: $(TOPPERS_INC_PATH)"
	@echo ""
	@echo "ASFLAGS: $(ASFLAGS)"
	@echo ""
	@echo "CFLAGS: $(CFLAGS)"
	@echo ""
	@echo "CXXFLAGS: $(CXXFLAGS)"
	@echo ""
	@echo "LDFLAGS: $(LDFLAGS)"
	@echo ""

# list all targets in makefile, idea from http://stackoverflow.com/questions/4219255/how-do-you-get-the-list-of-targets-in-a-makefile
.PHONY: no_targets__ list
no_targets__:
list:
	sh -c "$(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | sort"

include $(ROOT)/ecrobot/ecrobot.mak
#  * specifies build targets to build nxtOsek project using ecrobot api
#  * and includes $(ROOT)/ecrobot/tool_gcc.mak in which it specifies c configuration


.PHONY: check_for_sg
check_for_sg:
	test -f $(TOPPERS_OSEK_ROOT_SG)/sg/sg || { echo "Error: missing sg.exe, which must be manually installed at $(TOPPERS_OSEK_ROOT_SG)/sg/" && echo "You can download it from: http://lejos-osek.sourceforge.net/download.htm" && exit 1; }

# uncomment next line to do a verbose build
#CFLAGS := $(CFLAGS) -v

# uncomment next line to do build a depencency file for each c/c++ source file
#CFLAGS := $(CFLAGS) -MD
# -> note: this option is already set by default in $(ROOT)/ecrobot/ecrobot.mak!!








